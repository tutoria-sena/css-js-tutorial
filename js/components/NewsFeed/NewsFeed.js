var NewsFeed = (function () {

  function getNewsFeedData() {
    // Set up request parameters
    var params = {
      country: 'us',
      apiKey: '8e63316767f14a0bba6e4fbca94276e2'
    };

    // Create an ajax get request to obtain data
    $.ajax({
      url: "https://cors-anywhere.herokuapp.com/https://newsapi.org/v2/top-headlines?country=co&pageSize=4&apiKey=8e63316767f14a0bba6e4fbca94276e2",
      type: "GET",
      data: utils.toQueryString(params),
      success(data, text) {
        //Resultado del api (4 elementos)
        var articles = data.articles;

        // Obtener elemento del markup(html) donde se van a colocar los datos
        var $newsFeed = $('.news-feed');

        // Recorrer array y mostrar articulos
        articles.forEach(function(article) {
          console.log(article);
          // Crear el markup del container de la imagen
          var imageContainer = $('<div>', {class: 'news-feed__image-container', html:$('<img />', {'src': article.urlToImage})});

          // Crear el markup de los elementos de texto
          var author = $('<span>', {class: 'news-feed__card-author', html:article.author});
          var description = $('<span>', {class: 'news-feed__card-description', html:article.description});
          var title = $('<span>', {class: 'news-feed__card-title', html:article.title});

          // Crear la tarjeta con los elementos necesatios
          var newsFeedCard = $('<div>', {class: 'news-feed__card', html:[imageContainer, author, description, title]});

          // Adicionar la tarjeta al elemento de html
          $newsFeed.append(newsFeedCard);
        });

      },
      error(request, status, error) {
        console.log('error', error)
      }
    });
  }

  function renderNewsFeed() {
    getNewsFeedData();
  }

  return {
    renderNewsFeed: renderNewsFeed
  };
})();

